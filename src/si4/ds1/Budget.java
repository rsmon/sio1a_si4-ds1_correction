package si4.ds1;

import java.util.Scanner;

public class Budget {

    public static void main(String[] args) {      
        
       //<editor-fold defaultstate="collapsed" desc="OBTENTION DU CLAVIER">
       
        Scanner clavier= new Scanner(System.in);
        
      //</editor-fold>
    
       //<editor-fold defaultstate="collapsed" desc="DECLARATION DES VARIABLES">
        
        // DONNEE
       
        float budgetInitial=0f;
        float montantAchat;
        
        // CUMUL et COMPTEUR
        
        float totalAchats=0;
        int   nbAchats=0;
       
        // RESULTAT
        
        float budgetRestant;
        
        //</editor-fold>
        
       //<editor-fold defaultstate="collapsed" desc="AFFICHAGE D'UN MESSAGE EXPLICATIF">
       
        System.out.println("Ce programme vous permet de vérifier que la liste des dépenses "+
                "que vous envisagez entre bien  dans un budget d'une valeur donnée.\n"
                );
        
        //</editor-fold>
        
       //<editor-fold defaultstate="collapsed" desc="SAISIE DU BUDGET INITIAL">
      
        System.out.println("Entrez la valeur du budget initial");
        budgetInitial=clavier.nextFloat();
       
        //</editor-fold>
       
       //<editor-fold defaultstate="collapsed" desc="INITIALISATION DU BUDGET RESTANT">
       
        budgetRestant=budgetInitial;
        
        //</editor-fold>
        
       //<editor-fold defaultstate="collapsed" desc="RE-AFFICHAGE DU BUDGET INITIAL SAISI">
    
       System.out.println("Vous disposez d'un budget de : "+budgetInitial+" €\n");
       
       //</editor-fold>
        
       //<editor-fold defaultstate="collapsed" desc="SAISIE DU MONTANT D'UN PREMIER ACHAT">
        
        System.out.println("Entrez le montant d'un achat ou 0 pour abandonner.");
        montantAchat=clavier.nextFloat();
        
       //</editor-fold>
     
       while( montantAchat!=0){
        
         //<editor-fold defaultstate="collapsed" desc="TRAITEMENT DE L'ACHAT">
            
           if ( montantAchat<=budgetRestant ){
                
              budgetRestant -= montantAchat;
              totalAchats   += montantAchat; nbAchats++;
              if (budgetRestant>0){
                  System.out.println("\nAprès cet achat il vous restera: "+ budgetRestant+" €");
              }
           }
           else {
                
              System.out.println("\nVous ne pouvez pas effectuer cet achat de: "+ montantAchat+" €.");
              System.out.println("Vous devez effectuer un achat d'un montant maximum de: "+
                                 budgetRestant+" €\n"
                                );
           }
          
          //</editor-fold>
           
         //<editor-fold defaultstate="collapsed" desc="SAISIE UN NOUVEL ACHAT">
            
           if( budgetRestant > 0 ) {
                
                System.out.println("Entrez le montant d'un nouvel achat ou 0 "+
                        "si la liste de vos achats est terminée."
                        );
                
                montantAchat=clavier.nextFloat();
            }
            else{ 
               
                montantAchat=0;
           
            }
           
          //</editor-fold>
        
       }   
                
       //<editor-fold defaultstate="collapsed" desc="AFFICHAGE DU RECAPITULATIF">
       
       if( totalAchats>0){
            
            System.out.println();
            System.out.println("Vous disposez d'un budget de : "+budgetInitial+" €\n");
            
            if (budgetRestant>0){
                
                System.out.println("Vous envisagez de dépenser: "+totalAchats+ " € en " + 
                                    nbAchats+" achat(s)."
                                  );
                System.out.println();
                System.out.println("Il vous restera alors: " + budgetRestant + " €\n");
            }
            else{
                
                System.out.println("Vous envisagez de dépenser la totalité de votre budget en "+
                                    nbAchats+" achat(s).\n");
            }
            
        }
        else{
            
            System.out.println("\nVous avez abandonné\n");
        }
       
        //</editor-fold>
    }
}



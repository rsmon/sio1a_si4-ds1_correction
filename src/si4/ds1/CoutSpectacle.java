package si4.ds1;

import java.util.Scanner;

public class CoutSpectacle {

    public static void main(String[] args) {
         
        // Obtention du clavier
        Scanner clavier= new Scanner(System.in);
        
        // Déclaration des variables
        
        // ENTREES
        int     nbAdultes;
        int     nbEnfants;
        int     nbEtudiants;
        String  jourPromo;
       
        //RESULTATS
        float   montant;
        float   prixMoyen;
          
        //<editor-fold defaultstate="collapsed" desc="A VOUS DE COMPLETER ">
       
        System.out.println("Nombre d'adultes?");
        nbAdultes=clavier.nextInt();
        
        System.out.println("Nombre d'enfants?");
         nbEnfants=clavier.nextInt();
        
        System.out.println("Nombre d'étudiants?");
        nbEtudiants=clavier.nextInt();
        
        //</editor-fold>
        
        System.out.println("Jour de promotion? (Répondre par oui ou non )");
        jourPromo=clavier.next();
       
        //<editor-fold defaultstate="collapsed" desc="A VOUS DE COMPLETER">
        
        montant=7f*nbAdultes+4f*nbEnfants+5.5f*nbEtudiants;
        
        if( jourPromo.equals( "oui" ) ) { montant=montant*0.8f;}
        
        prixMoyen=montant/(nbEnfants+nbAdultes+nbEtudiants);
        
        System.out.printf("\nMontant dû: %3.2f €\n",montant);
        System.out.printf("\nPrix moyen d'une place: %3.2f €\n\n",prixMoyen);
        
        //</editor-fold>
                
    }
}


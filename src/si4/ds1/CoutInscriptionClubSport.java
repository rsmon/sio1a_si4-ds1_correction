
package si4.ds1;

import java.util.Scanner;

public class CoutInscriptionClubSport {

    public static void main(String[] args) {
        
        // OBTENTION DU CLAVIER
       
        Scanner clavier= new Scanner(System.in);
        
        // DECLARATION DES VARIABLES EN ENTREE
        
         float    montantAssurance=80;
         int      age;
         String   reponse="non";
         
        // RESULTAT
         
         float prixInscription;
         float totalInscripPlusAssur;
         
         float montantAPayer;
         
         System.out.println("Age?");
         age=clavier.nextInt(); 
         
         
         if (age<=8){
            prixInscription=45;
         }
         else if (age <=12){
            prixInscription=60;
         }
         else if (age <=15){
            prixInscription=75;
         }
         else{
            prixInscription=90;
         }
         
         System.out.printf("\nLe prix de l'inscription est de:   %3.2f € \n",prixInscription);
         System.out.printf("Le prix de l'assurance   est de:   %3.2f € \n",montantAssurance);
         
         totalInscripPlusAssur=prixInscription+montantAssurance;
         
         System.out.printf("Total:                            %3.2f €\n",totalInscripPlusAssur);
           
         System.out.println("\nSouhaitez vous payer en trois fois?\n");
         reponse=clavier.next();
         
         if (reponse.equals("oui")){
         
             montantAPayer=totalInscripPlusAssur/3;
           
             System.out.printf("\nVous payez en trois fois ,le 15 Septembre,"+
                               " le 15 Janvier et le 15 Avril, la somme de:  %3.2f €\n",montantAPayer
                              );
         } 
         else{
             
             System.out.printf("\nVous payer en une fois le 15 Septembre la somme de: %3.2f €\n",
                               totalInscripPlusAssur
                              );  
         }
         
         System.out.println();
         
    }
}



